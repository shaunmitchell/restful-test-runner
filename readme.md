# RESTful Katalon Test Suite Runner
a flask application for running katalon test suites using the katalon runtime
engine

---

## usage

```sh
$ ./krest.py --help
usage: krest.py [-h] [-p PORT] [-b HOST] -c KATALONC [-k KATALON_PROJECT] [-u]
                [--profile PROFILE] [--browser BROWSER] [--organization-id ORGANIZATION_ID]
                [--kre-license KRE_LICENSE] [-d LOG_DIRECTORY] [-v]

katalon server api

options:
  -h, --help            show this help message and exit
  -p PORT, --port PORT  port to run the server on
  -b HOST, --host HOST  host to run the server on
  -c KATALONC, --katalonc KATALONC
                        path to the katalonc executable
  -k KATALON_PROJECT, --katalon-project KATALON_PROJECT
                        path to the katalon project
  -u, --update-drivers  automatically update drivers before launching a test suite
  --profile PROFILE     default execution profile
  --browser BROWSER     default browser type
  --organization-id ORGANIZATION_ID
                        organization id
  --kre-license KRE_LICENSE
                        kre license (consider using the KRE_LICENSE environment variable)
  -d LOG_DIRECTORY, --log-directory LOG_DIRECTORY
                        directory to store log files
  -v, --verbose         increase verbosity
```

## endpoints

### @app.route('/api/v1/test-suite/run', methods=['POST', 'GET'])

Requires the name of a test suite to be passed with an optional
execution profile, browser type, and katalon runtime engine license.

#### parameters
- `name` the name of the test suite to run
- `project` *(optional | required)* the project filepath or repository url.
   optional if the server is configured to use a default project, else required
- `profile` *(optional)* the execution profile to use (defaults to DEV)
- `browser` *(optional)* the browser type to use (defaults to Chrome (headless))
- `kreLicense` *(optional)* the license to use (defaults to the license loaded on the server)
- `organization` *(optional)* the organization id to use (defaults to the organization loaded on the server)

#### sample response
```json
{
  "message": "test suite started", 
  "payload": {
    "browser": "Chrome (headless)", 
    "id": "20220316-225224-BAR", 
    "profile": "DEV", 
    "testSuite": "BAR", 
    "time": 1647485544.9933522
  }, 
  "success": true
}
```

### @app.route('/api/v1/test-suite/status', methods=['POST', 'GET'])

Check the status of a running test suite

#### parameters
- `id` the identifier of the test suite returned by */api/v1/test-suite/run*

#### sample response
```json
{
  "message": null, 
  "payload": {
    "runTime": 4.87941312789917, 
    "status": 2
  }, 
  "success": true
}
```

*`status` is one of (-1) stopped, (0) created, (1) started, or (2) done*  
*`runTime` is in seconds*

### @app.route('/api/v1/test-suite/output', methods=['POST', 'GET'])

check the `katalonc` output of a running test suite

#### parameters
- `id` the identifier of the test suite returned by */api/v1/test-suite/run*

#### sample response
```
Katalon workspace folder is set to default location: C:\Users\foo\AppData\Local\Temp\session-c7acef6f
Starting Groovy-Eclipse compiler resolver. Specified compiler level: unspecified
138 org.codehaus.groovy_2.4.20.v202009301404-e2006-RELEASE ACTIVE


INFO: Katalon Version: 8.2.5
INFO: Command-line arguments: -v -runMode=console -projectPath=C:/Users/foo/code/git/KatalonProject -retry=0 -testSuitePath=Test Suites/BAR -reportFileName=20220316-231810-BAR -executionProfile=DEV -browserType=Chrome (headless) -apiKey=********
INFO: User working dir: C:\Users\foo\git\restful-test-runner
INFO: Error log: C:/Users/foo/AppData/Local/Temp/session-c7acef6f/.metadata/.log
INFO: Katalon KatOne server URL: https://admin.katalon.com
INFO: Katalon TestOps server URL: https://testops.katalon.io
INFO: Katalon Store server URL: https://store.katalon.com
...
```

### @app.route('/api/v1/test-suite/stop', methods=['POST', 'GET'])
Stop the job with the given identifier

#### parameters
- `id` the id of the test run to stop

#### sample response
```json
{
  "message": "20220316-231810-BAR stopped", 
  "payload": {
    "id": "20220316-231810-BAR",
    "status": -1
  },
  "success": true
}
```

*`status` is one of (-1) stopped, (0) created, (1) started, or (2) done*

### @app.route('/api/v1/info', methods=['GET'])
Provides the status of the server--number of tests in the queue and
whether or not a test is currently running--and some configuration details.

#### sample response
```json
{
  "message": "server info", 
  "payload": {
    "queueSize": 2,
    "active": "20220316-231810-BAR"
  }, 
  "success": true
}
```

*`active` is null if no job is running*
