#!/usr/bin/env python3
#-*- coding: utf-8 -*-
#
# EXIT CODES:
# - 0: success
# - 2: no ssl certificate or key provided
# - 3: ssl certificate or key not found
#
# TODO:
# - add to server info: versions of katalon, kre, java, groovy, and chromedriver
# - add an option for a repository url + branch
# - add a config endpoint
# - make the project an optional server configuration without a default
# - use the default flask logger? https://stackoverflow.com/a/18379764/6150131
# - cleanup

import argparse
from cgitb import enable
import flask
import json
import os
import platform
import signal
import simplelog
import ssl
import subprocess
import sys
import time

from _io import TextIOWrapper
from argparse import RawTextHelpFormatter
from collections import namedtuple
from flask import request, jsonify
from pathlib import Path
from queue import Queue
from ssl import SSLContext, PROTOCOL_TLS
from subprocess import Popen
from testsuiterun import TestSuiteRun
from threading import Thread

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.log import enable_pretty_logging
from tornado.wsgi import WSGIContainer

app = flask.Flask("Katalon Test Suite Rest API")
app.config["DEBUG"] = os.environ.get("KATALON_REST_DEBUG", "false") != "false"

parser = argparse.ArgumentParser()
parser.description = "katalon server api"
parser.epilog = """
Starts a RESTful API server that runs Katalon Studio test suites using the
Katalon Runtime Engine. A Katalon Runtime Engine license key is required and
can be set using (in order of highest to lowest priority):
    (1) the --kre-license command line argument,
    (2) a .kre.license file in the current directory, or
    (3) the KRE_LICENSE environment variable
"""
parser.add_argument("-p", "--port", default=5000, type=int,
                    help="port to run the server on")
parser.add_argument("-b", "--host", default="localhost", type=str,
                    help="host to run the server on")
parser.add_argument("-c", "--katalonc", default="./katalonc", required=True, type=Path,
                    help="path to the katalonc executable")
parser.add_argument("-k", "--katalon-project", default=".", type=Path,
                    help="path to the katalon project")
parser.add_argument("-u", "--update-drivers", action="store_true", default=False,
                    help="automatically update drivers before launching a test suite")
parser.add_argument("--profile", default="DEV", type=str,
                    help="default execution profile")
parser.add_argument("--browser", default="Chrome (headless)", type=str,
                    help="default browser type")
parser.add_argument("--organization-id", default="", type=str,
                    help="organization id")
parser.add_argument("--kre-license", default="", type=str,
                    help="kre license (consider using the KRE_LICENSE environment variable)")
parser.add_argument("-t", "--token-file", default=".tokens.json", type=Path,
                    help="path to the token json file for authentication")
parser.add_argument("-T", "--no-tokens", action="store_true", default=False,
                    help="disable token authentication")
parser.add_argument("-w", "--whitelist-file", default=".whitelist.json", type=Path,
                    help="path to the whitelist json file for whitelisted ip addresses")
parser.add_argument("-W", "--no-whitelist", action="store_true", default=False,
                    help="disable whitelisting")
parser.add_argument("-d", "--log-directory", default="./logs", type=Path,
                     help="directory to store log files")
parser.add_argument("-C", "--ssl-certificate", default=None, type=Path,
                     help="ssl certificate public key")
parser.add_argument("-K", "--ssl-key", default=None, type=Path,
                     help="ssl certificate private key")
parser.add_argument("-S", "--no-ssl", action="store_true", default=False,
                    help="disable ssl")
parser.add_argument("--server-threads", default=4, type=int,
                    help="number of threads to run tornado server with")
parser.add_argument("--debug", action="store_true", default=False,
                    help="run the flask server in debug mode")
parser.add_argument("-v", "--verbose", action="count", default=1,
                    help="increase verbosity")
parser.add_argument("-q", "--quiet", action="store_true", default=False,
                    help="do not print any output")
args = parser.parse_args()

if args.debug:
    app.config["DEBUG"] = True

class MissingLicenseException(Exception): pass
class MissingOrganizationException(Exception): pass

# function to get the kre license key from:
#  - the --kre-license command line argument
#  - a .kre.license file in the current directory
#  - the KRE_LICENSE environment variable
def get_kre_license() -> str:
    kre_license: str = args.kre_license
    if not kre_license and os.path.exists(".kre.license"):
        kre_license = Path('.kre.license').read_text()
    if not kre_license:
        kre_license = os.environ.get('KRE_LICENSE', '')
    return kre_license.strip()

# function to get the organization id from:
# - the --organization-id command line argument
# - a .organization.id file in the current directory
# - the KRE_ORGANIZATION_ID environment variable
def get_organization_id() -> str:
    organization_id: str = args.organization_id
    if not organization_id and os.path.exists(".organization.id"):
        organization_id = Path('.organization.id').read_text()
    if not organization_id:
        organization_id = os.environ.get('KRE_ORGANIZATION_ID', '')
    return organization_id

# watch the queue for new test suite runs and then run them
def monitor_queue():
    simplelog.info(f"queue monitor started")
    while True:
        simplelog.debug(f"queue monitor waiting for new test suite run")
        try:
            # fetch the next test suite to run from the queue
            test_run: TestSuiteRun = TEST_RUN_QUEUE.get(block=True)
            # allow cleanly exiting the monitor queue if None is added
            if test_run == None:
                simplelog.info(f"queue monitor received None, exiting")
                return
            simplelog.info(f"fetched test run {test_run.identifier} from the queue")
            # start the test suite
            try:
                test_run.start(args.katalonc)
                # add the test suite to the running list
                TEST_RUNS[test_run.identifier] = test_run
                # wait for the test suite to finish
                test_run.wait()
            except Exception as e:
                simplelog.error(f"error starting test run {test_run.identifier}: {e}")
        except Exception:
            pass

# function for returning consistent json responses
def build_response(success: bool,
                   message: str | None = None,
                   payload: dict[object, object] | list[object] | None = None) -> str:
    '''
    Builds a json response with the given success, message, and payload
    '''
    if payload == None:
        payload = {}

    response: dict[str, object] = {
        "success": success,
        "message": message,
        "payload": payload
    }

    return jsonify(response)

def load_json(path: Path, missing_ok: bool = False) -> dict:
    '''
    Loads a json file from the given path and returns the dictionary. If the
    file does not exist and missing_ok is False, an exception is raised.
    '''
    json_data: dict[str, object] = {}
    if path.exists():
        with path.open() as f:
            json_data = json.load(f)
    elif not missing_ok:
        raise FileNotFoundError(f"file not found: {path}")
    return json_data

@app.before_request
def validate_request():
    '''
    Validate a token in the request parameters and check the ip address against
    the whitelist.
    '''
    if not args.no_tokens:
        if request.method == 'POST':
            data: dict[str, str] = request.get_json()
        elif request.method == 'GET':
            data: dict[str, str] = request.args
        request_token: str = data.get('token', '')
        tokens: dict[str, str] = load_json(args.token_file, missing_ok=True)
        if request_token not in tokens:
            return build_response(False, "unauthorized access"), 403

    if not args.no_whitelist:
        request_ip: str = request.remote_addr
        whitelist: dict[str, str] = load_json(args.whitelist_file, missing_ok=True)
        if request_ip not in whitelist:
            return build_response(False, "unauthorized access"), 403

@app.before_request
def log_request():
    '''
    Log each request to the main log file
    '''
    if not app.config["DEBUG"]:
        # log the ip addres, request method, request path, response code, and user agent
        simplelog.info(f"{request.remote_addr} -- {request.method} {request.path} -- {request.user_agent}")

@app.route('/', methods=['POST', 'GET'])
def home():
    return build_response(True, "hello"), 200

@app.route('/api/v1/test-suite/run', methods=['POST', 'GET'] if app.config['DEBUG'] else ['POST'])
def test_suite_run():
    '''
    Requires the name of a test suite to be passed with an optional
    execution profile, browser type, and api key.
    
    Request Parameters:
    - name: the name of the test suite to run
    - project: the project filepath or repository url
    - profile: the execution profile to use (defaults to DEV)
    - browser: the browser type to use (defaults to Chrome (headless))
    - kreLicense: the license to use (defaults to the license loaded on the server)
    - organization: the organization id to use (defaults to the organization loaded on the server)
    '''
    if request.method == 'POST':
        data: dict[str, str] = request.get_json()
    elif request.method == 'GET':
        data: dict[str, str] = request.args
    else:
        return build_response(False, "invalid request method"), 400

    # check if the test suite name is provided
    if 'name' not in data:
        return build_response(False, "test suite name not provided"), 400

    # check if a default project is set and, if not, is provided
    if not args.katalon_project and 'project' not in data:
        return build_response(False, "project not provided"), 400

    # create the test suite run
    test_run: TestSuiteRun = TestSuiteRun(
        data['name'],
        data.get('project', args.katalon_project),
        data.get('browser', args.browser),
        data.get('profile', args.profile),
        data.get('kreLicense', KRE_LICENSE),
        data.get('organization', args.organization_id)
    )

    # determine whether the test suite exists
    status: str = ""
    success: boolean = False
    payload: dict[str, object] = {}
    if not test_run.exists():
        status = "doesn't exist"
    else:
        # we will successfully queue the test run at this point
        success = True

        # check to see if another test suite is already running
        run_is_active: bool = False
        run_ids: list[str] = sorted(TEST_RUNS.keys())
        if len(run_ids) > 0:
            last_run_id: str = sorted(TEST_RUNS.keys())[-1]
            last_run: TestSuiteRun = TEST_RUNS[last_run_id]
            run_is_active = last_run.status == TestSuiteRun.RUNNING
        if run_is_active:
            status = "queued"
        else:
            status = "started"

        # add it to the queue
        TEST_RUN_QUEUE.put(test_run)

        # build the payload
        payload = {
            "id": test_run.identifier,
            "testSuite": test_run.name,
            "time": test_run.start_time,
            "profile": test_run.profile,
            "browser": test_run.browser
        }

    return build_response(success,
        f"test suite '{test_run.name}' {status}",
        payload), 200

@app.route('/api/v1/test-suite/status', methods=['POST', 'GET'])
def test_suite_status():
    '''
    Check the status of a running test suite

    Request Parameters:
    - id: the identifier of the test suite returned by /api/v1/test-suite/run
    '''
    if request.method == 'POST':
        data: dict[str, str] = request.get_json()
    elif request.method == 'GET':
        data: dict[str, str] = request.args
    else:
        return build_response(False, "invalid request method"), 400

    # get the test run with the given identifier
    test_run: TestSuiteRun = TEST_RUNS.get(data['id'], None)

    # if the test run does not exist, return an error
    if test_run is None:
        return build_response(False, "test run not found"), 400

    # return the test run status
    duration: float
    if test_run.status == TestSuiteRun.RUNNING:
        duration = time.time() - test_run.start_time
    else:
        duration = test_run.duration
    return build_response(
        True,
        f"{test_run.identifier} status {test_run.status}",
        payload={
            "status": test_run.status,
            "runTime": duration
    }), 200

@app.route('/api/v1/test-suite/output', methods=['POST', 'GET'])
def test_suite_output():
    '''
    Check the current output of a running test suite

    Request Parameters:
    - id: the identifier of the test suite returned by /api/v1/run/test-suite
    '''
    if request.method == 'POST':
        data: dict[str, str] = request.get_json()
    elif request.method == 'GET':
        data: dict[str, str] = request.args
    else:
        return build_response(False, "invalid request method"), 400

    # get the test run with the given identifier
    test_run: TestSuiteRun = TEST_RUNS.get(data['id'], None)

    # if the test run does not exist, return an error
    if test_run is None:
        return build_response(False, "test run not found"), 400

    return test_run.output

@app.route('/api/v1/test-suite/stop', methods=['POST', 'GET'])
def test_suite_stop():
    '''
    Stop the job with the given identifier

    Request Parameters:
    - id: the id of the test run to stop
    '''
    if request.method == 'POST':
        data: dict[str, str] = request.get_json()
    elif request.method == 'GET':
        data: dict[str, str] = request.args

    # require a test run id
    if 'id' not in data:
        return build_response(False, "test run id not provided"), 400

    # get the test run with the given identifier
    test_run: TestSuiteRun = TEST_RUNS.get(data['id'], None)
    if test_run is None:
        return build_response(False, "test run not found"), 400
    test_run.proc.kill()

    # check if we successfully stopped the process
    success: bool
    message: str
    try:
        test_run.proc.wait(timeout=5)
    except subprocess.TimeoutExpired:
        success = False
    else:
        success = test_run.proc.poll() is not None

    if success:
        test_run.status = TestSuiteRun.STOPPED
        message = f"{test_run.identifier} stopped"
    else:
        message = "failed to stop test run"

    return build_response(
        success,
        message,
        payload={
            "id": test_run.identifier,
            "status": test_run.status
    }), 200

@app.route('/api/v1/info')
def info():
    '''
    Provides the status of the server--number of tests in the queue and
    whether or not a test is currently running--and some configuration details.
    '''
    # check for an actively running test run
    active: str | None = None
    run_ids: list[str] = sorted(TEST_RUNS.keys())
    if len(run_ids) > 0:
        last_run_id: str = sorted(TEST_RUNS.keys())[-1]
        last_run: TestSuiteRun = TEST_RUNS[last_run_id]
        active = last_run.identifier if last_run.status == TestSuiteRun.RUNNING else None

    return build_response(True,
        "server status",
        payload={
            "queueSize": TEST_RUN_QUEUE.qsize(),
            "active": active,
            "defaults": {
                "project": args.katalon_project.name,
                "browser": args.browser,
                "profile": args.profile,
            }
    }), 200

def tornado_shutdown():
    TEST_RUN_QUEUE.put_nowait(None)
    time.sleep(0.25)
    simplelog.log("...exiting")
    IOLoop.instance().stop()
    sys.exit()

## START THE SERVER ##

def main():
    # if we're in debug mode, use flask, else use tornado
    server_type: str = 'flask'
    if not app.config['DEBUG']:
        server_type = 'tornado'

    # set up logging
    if args.quiet:
        if server_type == 'tornado':
            import logging
            log = logging.getLogger('werkzeug')
            log.disabled = True
        app.logger.disabled = True
        simplelog.VERBOSITY = 0
    else:
        if server_type == 'tornado':
            enable_pretty_logging()
        simplelog.LOGFILE = args.log_directory / "app.log"
        simplelog.VERBOSITY = args.verbose
        simplelog.DEFAULT_STDOUT = app.config["DEBUG"] or args.verbose > 1
        simplelog.log("starting...")

    # set up the license
    ## verify that a kre license is provided
    if not KRE_LICENSE:
        raise MissingLicenseException("No KRE license provided")
    ## don't log the license
    simplelog.DEFAULT_REDACT_VALUES = [KRE_LICENSE]
    simplelog.log("loaded KRE license '" + KRE_LICENSE[:3] + "****" + KRE_LICENSE[-3:] + "'")

    # check for a kre license set on the command line
    if args.kre_license:
        warning: str = "WARNING: using a kre license key on the command line "
        warning +=     "exposes it to other users on the system. Consider using "
        warning +=     "the KRE_LICENSE environment variable or a .kre.license "
        warning +=     "file in the current directory instead."
        simplelog.warn(warning)

    # start the app
    simplelog.log(f"launching {server_type} server")

    ## setup ssl
    ssl_options: str | tuple[str, str] | SSLContext | dict[str, str] | None = None
    if args.no_ssl:
        simplelog.warn("ssl: disabled")
    elif args.ssl_certificate and args.ssl_key:
        # check that the cert and key exist
        if not args.ssl_certificate.is_file() or not args.ssl_key.is_file():
            simplelog.error("ssl error: couldn't load ssl certificate")
            simplelog.error(f"{args.ssl_certificate} exists: {args.ssl_certificate.is_file()}")
            simplelog.error(f"{args.ssl_key} exists: {args.ssl_key.is_file()}")
            sys.exit(3)
        simplelog.log("ssl: enabled")
        simplelog.log(f"ssl cert -- {args.ssl_certificate}")
        simplelog.log(f"ssl key  -- {args.ssl_key}")
        if server_type == 'flask':
            ssl_options = (args.ssl_certificate, args.ssl_key)
        elif server_type == 'tornado':
            ssl_options = {"certfile": args.ssl_certificate, "keyfile": args.ssl_key}
    else:
        if server_type == 'flask':
            # use adhoc ssl for flask
            ssl_options = "adhoc"
            simplelog.log("ssl: adhoc")
        else:
            # for any other production server, require the --no-ssl flag
            simplelog.error("ssl error: no certificate or key provided!")
            simplelog.error("           to launch in production mode without")
            simplelog.error("           http, use the --no-ssl flag")
            sys.exit(2)
    # start a thread to monitor the queue for new test runs
    simplelog.log("starting queue monitoring thread")
    monitor_thread: Thread = Thread(target=monitor_queue)
    monitor_thread.start()
    if server_type == 'flask':
        app.run(host=args.host, port=args.port, ssl_context=ssl_options)
    elif server_type == 'tornado':
        tornado_server: HTTPServer = HTTPServer(WSGIContainer(app), ssl_options=ssl_options)
        tornado_server.bind(args.port, args.host)
        if platform.system() == "Windows" and args.server_threads > 1:
            simplelog.log("Windows does not support multiple server threads -- defaulting to 1 thread")
            args.threads = 1
        simplelog.log(f"starting server at http{'s' if ssl_options else ''}://{args.host}:{args.port}")
        # register a shutdown hook to kill tornado on ctrl-c
        ioloop: IOLoop = IOLoop.current()
        signal.signal(signal.SIGINT, lambda sig, frame: ioloop.add_callback_from_signal(tornado_shutdown))
        # start the server
        tornado_server.start(args.threads)
        ioloop.start()

if __name__ == "__main__":
    try:
        # set up some global vars
        KRE_LICENSE: str = get_kre_license()
        # holds all active and completed test runs
        TEST_RUNS: dict[str, TestSuiteRun] = {}
        # holds all test runs that are waiting to be run
        TEST_RUN_QUEUE: Queue[TestSuiteRun] = Queue()

        main()
    except KeyboardInterrupt as e:
        simplelog.log("keyboard interrupt received", log_level=simplelog.DEBUG)
        TEST_RUN_QUEUE.put_nowait(None)
        time.sleep(0.25)
        simplelog.log("...exiting")
        sys.exit()
    except Exception as e:
        simplelog.error("unhandled exception: " + str(e))
        TEST_RUN_QUEUE.put_nowait(None)
        time.sleep(0.25)
        simplelog.log("...exiting")
        sys.exit()
