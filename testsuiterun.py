import simplelog
import subprocess
import time

from _io import TextIOWrapper
from pathlib import Path
from threading import Thread
from subprocess import Popen

class TestSuiteRun:
    STOPPED: int = -1
    CREATED: int = 0
    RUNNING: int = 1
    COMPLETED: int = 2

    def __init__(self, name: str, project: str, browser: str, profile: str,
                 kre_license: str, organization_id: str,
                 update_drivers: bool = False):
        self.name = name
        self.project = project
        self.browser = browser
        self.profile = profile
        self.kre_license = kre_license
        self.organization_id = organization_id
        self.update_drivers = update_drivers
        self.proc: Popen[str] = None
        self.status: int = TestSuiteRun.CREATED
        self.start_time: float = time.time()
        self.end_time: float = 0
        self._output: str = ""
        self._log_file: TextIOWrapper | None = None
        simplelog.log(f"Created test suite run {self.name} with ident: {self.identifier}")

    @property
    def identifier(self) -> str:
        # generate a unique identifier for the run
        return time.strftime('%Y%m%d-%H%M%S', time.localtime(self.start_time)) + '-' + self.name

    @property
    def output(self) -> str:
        return self._output

    @property
    def duration(self) -> float:
        if self.end_time:
            return self.end_time - self.start_time
        return -1

    @property
    def path(self) -> Path:
        return Path(self.project, "Test Suites", self.name + ".ts")

    def start(self, katalonc: Path):
        test_args: list[str] = [
             '-v',
             '-noSplash',
             '-runMode=console',
            f'-projectPath={self.project}',
             '-retry=0',
            f'-testSuitePath=Test Suites/{self.name}',
            f'-reportFileName={self.identifier}',
            f'-executionProfile={self.profile}',
            f'-browserType={self.browser}',
            f'-apiKey={self.kre_license}'
        ]
        # if --update-drivers is set, add an option to update the drivers
        if self.update_drivers:
            test_args.extend(["--config", "-webui.autoUpdateDrivers=true"])
        # if an organization id is provided, add that
        if self.organization_id:
            test_args.append(f"-organizationId='{self.organization_id}'")
        simplelog.log(f"Starting test suite {self.name} with args: {test_args}")
        self.proc = subprocess.Popen(
            [katalonc, *test_args],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT, # merge stdout and stderr
            universal_newlines=True
        )
        # start fetching the stdout
        stdout_thread: Thread = Thread(target=self._consume_stdout)
        stdout_thread.start()
        # update start status
        self.status = TestSuiteRun.RUNNING
        self.start_time = time.time()

    def wait(self):
        self.proc.wait()
        self.__exit__()

    def exists(self):
        return self.path.is_file()

    def _setup_logging(self):
        log_path: Path = simplelog.LOGFILE.parent / f"{self.identifier}.log"
        # create the log directory if it doesn't exist
        log_path.parent.mkdir(parents=True, exist_ok=True)
        self._log_file = open(log_path, "w")

    def _consume_stdout(self):
        self._setup_logging()
        for line in iter(self.proc.stdout.readline, ''):
            # TODO: don't log so much
            simplelog.log(f"{self.identifier} -- {line.strip()}", stdout=False, log_level=simplelog.DEBUG)
            self._log_file.write(line)
            self._output += line
        self.__exit__()

    def __exit__(self):
        self.end_time = time.time()
        if self._log_file and not self._log_file.closed:
            self._log_file.close()
        simplelog.log(f"Test suite {self.name} completed in {self.duration} seconds")
        self.status = TestSuiteRun.COMPLETED
