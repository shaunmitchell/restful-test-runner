import sys
import time

from pathlib import Path

# log levels
QUIET: int = 0
ERROR: int = 1
WARN:  int = 2
INFO:  int = 3
DEBUG: int = 4

# default variables
LOGFILE: Path = Path("log.txt")
DEFAULT_STDOUT: bool = False
DEFAULT_LOGLEVEL: int = INFO
DEFAULT_REDACT_VALUES: list[str] = []
VERBOSITY: int = 1

def log(*args: object, log_level: int | None = None, stdout: bool | None = None, redact_values: list[str] | None = None) -> None:
    '''
    Logs a message with the given log level to simplelog.LOGFILE. If stdout is
    set to true, the message will also be printed to stdout (or to stderr if
    the log level is ERROR or WARN).
    '''
    # set parameters to default values if not provided
    if log_level is None:
        log_level = DEFAULT_LOGLEVEL
    if stdout is None:
        stdout = DEFAULT_STDOUT
    if redact_values is None:
        redact_values = DEFAULT_REDACT_VALUES

    if log_level <= VERBOSITY and VERBOSITY != QUIET:
        # create the log directories if they don't exist
        LOGFILE.parent.mkdir(parents=True, exist_ok=True)

        # join the logged args into a string
        message: str = " ".join([str(item) for item in args])

        # remove any redacted values
        for value in redact_values:
            message = message.replace(value, "***")

        # add a timestamp
        timestamp: str = time.strftime("%Y-%m-%d %H:%M:%S")
        message = f"[{timestamp}] {message}"

        # log to the logfile and optionally stdout/stderr
        if stdout:
            if log_level == ERROR:
                print(message, file=sys.stderr, flush=True)
            else:
                print(message, flush=True)
        with LOGFILE.open("a") as logfile:
            logfile.write(f"{message}\n")

def error(*args: object, stdout: bool | None = None, redact_values: list[str] | None = None) -> None:
    return log(*args, log_level=ERROR, stdout=stdout, redact_values=redact_values)

def warn(*args: object, stdout: bool | None = None, redact_values: list[str] | None = None) -> None:
    return log(*args, log_level=WARN, stdout=stdout, redact_values=redact_values)

def info(*args: object, stdout: bool | None = None, redact_values: list[str] | None = None) -> None:
    return log(*args, log_level=INFO, stdout=stdout, redact_values=redact_values)

def debug(*args: object, stdout: bool | None = None, redact_values: list[str] | None = None) -> None:
    return log(*args, log_level=DEBUG, stdout=stdout, redact_values=redact_values)

def get_config() -> dict[str, object]:
    return {
        "LOGFILE": LOGFILE,
        "DEFAULT_STDOUT": DEFAULT_STDOUT,
        "DEFAULT_LOGLEVEL": DEFAULT_LOGLEVEL,
        "DEFAULT_REDACT_VALUES": DEFAULT_REDACT_VALUES,
        "VERBOSITY": VERBOSITY
    }